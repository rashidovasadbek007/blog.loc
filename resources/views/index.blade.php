@extends('layouts.head')

@section('content')
    <div id="app">
        <navbar></navbar>
        <router-view></router-view>
    </div>
@endsection
